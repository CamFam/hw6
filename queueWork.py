rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)
q = HotQueue("queue", host='172.17.0.1', port=6379, db=1)

def _generate_jid():
    return str(uuid.uuid4())

def _generate_job_key(jid):
    return 'job.{}'.format(jid)
	
def _instantiate_job(jid, status, start, end):
	#create a job object, will be used in function add_job
    if type(jid) == str:
        return {'id': jid,
                'status': status,
                'start': start,
                'end': end
        }
    return {'id': jid.decode('utf-8'),
            'status': status.decode('utf-8'),
            'start': start.decode('utf-8'),
            'end': end.decode('utf-8')
    }

def _save_job(job_key, job_dict):
    #Save a job object in the Redis database.
	rd.hmset(job_ley, job_dict)

def _queue_job(jid):
    #Add a job to the redis queue."""
    q.put(jid)

def add_job(start, end, status="submitted"):
    #Save/Queue a job.
    jid = generate_jid()
    job_dict = instantiate_job(jid, status, start, end)
    save_job(jid, job_dict)
    queue_job(jid)
    return job_dict

def update_job_status(jid, status):
    #Update the status of job with job id `jid` to status `status`.
    jid, status, start, end = rd.hmget(generate_job_key(jid), 'id', 'status', 'start', 'end')
    job = _instantiate_job(jid, status, start, end)
    if job:
        job['status'] = status
        _save_job(_generate_job_key(jid), job)
    else:
        raise Exception()

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

@app.route('/jobs', methods=['POST'])
def jobs_api():
    try:
        job = request.get_json(force=True) #job will be a JSON object with start and end
    except Exception as e:
        return True, json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)})
    return json.dumps(jobs.add_job(job['start'], job['end']))
